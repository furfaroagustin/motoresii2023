using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodigoGuns : MonoBehaviour
{
    public GameObject armaPistola;
    public GameObject armaEscopeta;
    public GameObject armaFusil;
    public GameObject armaRPG;

    void Start()
    {
        armaPistola.SetActive(false);
        armaEscopeta.SetActive(false);
        armaFusil.SetActive(false);
        armaRPG.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "arma1")
        {
            armaPistola.SetActive(true);
            armaEscopeta.SetActive(false);
            armaFusil.SetActive(false);
            armaRPG.SetActive(false);
        }
        if (other.tag == "arma2")
        {
            armaPistola.SetActive(false);
            armaEscopeta.SetActive(true);
            armaFusil.SetActive(false);
            armaRPG.SetActive(false);
        }
        if (other.tag == "arma3")
        {
            armaPistola.SetActive(false);
            armaEscopeta.SetActive(false);
            armaFusil.SetActive(true);
            armaRPG.SetActive(false);
        }
        if (other.tag == "arma4")
        {
            armaPistola.SetActive(false);
            armaEscopeta.SetActive(false);
            armaFusil.SetActive(false);
            armaRPG.SetActive(true);
        }
    }

}
