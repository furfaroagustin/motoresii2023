using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class RedPlayer : MonoBehaviour
{
    public MonoBehaviour[] codigosIgnorar;//array[ arreglo: varios objetos con el mismo tipo de variable)
    private PhotonView photonView;// componente agregado al personaje 
  
    private void Start()
    {// saca el componente, se crea la variable pero no se le agrega el valor y se le agegan los atributos
        photonView = GetComponent<PhotonView>();
        if (!photonView.IsMine)//is mine =si photon view es falso todos los objetos se deshabilitan, asi funciona solo un player
        {
            foreach (var codigo in codigosIgnorar)
            {
                codigo.enabled = false;
            }
        }
        
    }
}
