using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class BarraMana : MonoBehaviour
{
    private Image barImage;
    private void Awake()
    {
        barImage = transform.Find("Bar").GetComponent<Image>();
        barImage.fillAmount = 3f;
    }

    public class  Mana
    {
        public const int mana_max = 100;
        private int manaAmount;
        private float manaRegenAmount;
        public Mana() 
        {
            manaAmount = 0;
            manaRegenAmount = 30f;

        }
    }
}
