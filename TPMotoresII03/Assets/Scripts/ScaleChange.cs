using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleChange : MonoBehaviour
{
    public float aumentarTamaņo;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale += new Vector3(aumentarTamaņo, aumentarTamaņo, 0) * Time.deltaTime;
    }
    //cambios>/aumento de escala
}
