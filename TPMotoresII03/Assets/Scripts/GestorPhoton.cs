using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//importante habilita photon
using Photon.Pun;
using Photon.Realtime;

public class GestorPhoton : MonoBehaviourPunCallbacks
{//habilita funciones de photom
    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();//verificacion para onectarse al servidor
    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(); 
    }//ya nos conectamos
    public override void OnJoinedLobby()//nos unimos a un lobby
    {
        PhotonNetwork.JoinOrCreateRoom("Cuarto", new RoomOptions { MaxPlayers = 5 }, TypedLobby.Default );
    }

    public override void OnJoinedRoom()//QUE PASA CIUANDO NOS UNIMOS AL LOBBY
    {
        PhotonNetwork.Instantiate("Player",new Vector2(Random.Range(20, 3), 20), Quaternion.identity);
        
    }//instanciar en punto aleatorio al personaje y que mantenga la rotacion 


}
//photonView identifica el objeto en el juego(en unity)