using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public ActivarArma tomarArma;//referencia al otro codigo
    public int numeroArma;//variable de nro entero
    
    void Start()
    {// sacxar codigo a personaje del jugador
        tomarArma = GameObject.FindGameObjectWithTag("Player").GetComponent<ActivarArma>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            tomarArma.ActivarArmas(numeroArma);
            Destroy(gameObject);

        }
    }
}
