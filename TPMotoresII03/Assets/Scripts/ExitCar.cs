using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitCar : MonoBehaviour
{
    public EntryCar entrarAuto;
    public GameObject camaraVehiculo;
    public GameObject jugador;
    public PlayerMove carMove;
    private void Start()
    {
        jugador = entrarAuto.jugador;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            salirVehiculo();
        }
    }
    public void salirVehiculo()
    {
        jugador.SetActive(true);
        jugador.transform.position = gameObject.transform.position;
        camaraVehiculo.SetActive(false);
        carMove.enabled = false;
        entrarAuto.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

}
