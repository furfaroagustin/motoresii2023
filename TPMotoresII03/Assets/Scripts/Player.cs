using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidad;
    public float fuerzaSalto;
    public float posX;
    public Rigidbody rb;
    
    void Start()
    {
        
    }


    void Update()
    {
        posX = Input.GetAxis("Horizontal");
        transform.position += (Vector3)new Vector2(posX * velocidad * Time.deltaTime, 0f);
        if(Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(transform.up * fuerzaSalto, ForceMode.Impulse);
        }
    }
}
