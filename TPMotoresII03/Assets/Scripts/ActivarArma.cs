using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarArma : MonoBehaviour
{
    public GameObject[] armas;//arreglo: crea un conjunto de game objet con armas
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ActivarArmas(int numero)//funcion de activar un numero que indica que arma se activa.
    {//la funcion primero desactiva todas las armas y lurgo toma cuantos objetos hayen el arreglo con el"lenht"
        for (int i = 0; i < armas.Length; i++)
        {
            armas[i].SetActive(false);
        }
        armas[numero].SetActive(true);
    }
}
