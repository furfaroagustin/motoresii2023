using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daño : MonoBehaviour
{
    public Vida barraJugador;
    public float daño = 5.0f;
    void Start()
    {

    }
    public void OnCollisionEnter(Collision colision)
    {
        if (colision.gameObject.CompareTag("Daño"))
        {
            barraJugador.vidaActual -= daño;
            Debug.Log("Daño");
        }
    }
}